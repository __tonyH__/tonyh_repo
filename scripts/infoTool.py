# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'infoTool.ui'
#
# Created: Fri Dec 07 19:21:49 2012
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import maya.cmds as cmd
import sip

path = __file__.split("\\")[0]
windowObject = "pyInfoToolWin"

def getMainWindow():
    import maya.OpenMayaUI as mui
    
    pointer = mui.MQtUtil.mainWindow()
    return sip.wrapinstance(long(pointer), QtCore.QObject)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_infoTool(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8(windowObject))
        Form.resize(400, 189)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        Form.setMinimumSize(QtCore.QSize(400, 189))
        Form.setMaximumSize(QtCore.QSize(400, 189))
        Form.setBaseSize(QtCore.QSize(400, 189))
        self.widget = QtGui.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(0, 0, 402, 189))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.riggingology_lb = QtGui.QLabel(self.widget)
        self.riggingology_lb.setText(_fromUtf8(""))
        self.riggingology_lb.setPixmap(QtGui.QPixmap(_fromUtf8(path+"/locPics/riggingology.jpg")))
        self.riggingology_lb.setObjectName(_fromUtf8("riggingology_lb"))
        self.verticalLayout.addWidget(self.riggingology_lb)
        self.infoTool_lb = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Verdana"))
        self.infoTool_lb.setFont(font)
        self.infoTool_lb.setFrameShape(QtGui.QFrame.NoFrame)
        self.infoTool_lb.setFrameShadow(QtGui.QFrame.Plain)
        self.infoTool_lb.setLineWidth(3)
        self.infoTool_lb.setMidLineWidth(3)
        self.infoTool_lb.setText(_fromUtf8("Custom Locator Tool developed by Antonio Sacco, character TD.      I hope you\'ll enjoy it! Please visit my blogspot: www.riggingology.blogspot.com"))
        self.infoTool_lb.setTextFormat(QtCore.Qt.PlainText)
        self.infoTool_lb.setScaledContents(False)
        self.infoTool_lb.setAlignment(QtCore.Qt.AlignCenter)
        self.infoTool_lb.setWordWrap(True)
        self.infoTool_lb.setMargin(5)
        self.infoTool_lb.setIndent(0)
        self.infoTool_lb.setOpenExternalLinks(True)
        self.infoTool_lb.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByKeyboard|QtCore.Qt.LinksAccessibleByMouse)
        self.infoTool_lb.setObjectName(_fromUtf8("infoTool_lb"))
        self.verticalLayout.addWidget(self.infoTool_lb)
        
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)


    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "infoUI", None, QtGui.QApplication.UnicodeUTF8))

class StepsDlg(QtGui.QDialog, Ui_infoTool):
    def __init__(self,parent=getMainWindow()):
        super(StepsDlg, self).__init__(parent)
        self.setupUi(self)
        
def showW():
    if cmd.window(windowObject,q=1, exists=1):
        cmd.deleteUI(windowObject)
    global gui
    gui = StepsDlg()
    gui.show()